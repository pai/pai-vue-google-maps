export default {
  props: {
    markers: {
      type: Array,
      default () {
        return []
      }
    },
    markerIcon: {
      type: String,
      default: '/static/img/marker_icon.png'
    },
    markerIconSmall: {
      type: String,
      default: '/static/img/marker_icon_small.png'
    }
  },
  data () {
    return {}
  },
  methods: {
    getMarker(lat, lon, icon, title, id) {
      return new this.googleMaps.Marker({
        position: {lat: lat, lng: lon},
        icon: icon,
        title: title,
        id: id
      })
    },
    drawMarker(lat, lon, icon, title, id) {
      let m = this.getMarker(lat, lon, icon, title, id)

      m.setMap(this.googleMap)

      return m
    }
  }
}
