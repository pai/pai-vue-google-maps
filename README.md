# pai-vue-google-maps

> Google maps on vuejs

This is alpha software and is under heavy development.

### Usage

Register vuejs plugin


``` javascript

import VueGoogleMaps from '@paiuolo/pai-vue-google-maps/plugins/VueGoogleMaps'

var options = {
  apiKey: 'xxx',
  libraries: 'places,..'
}

Vue.use(VueGoogleMaps, options)

```

### Notes

Browser MUST support Promise

