export default {
  data () {
    return {
      geocoder: null
    }
  },
  methods: {
    geocodePosition(position) {
      this.geocoder.geocode({'location': {lat: position.lat, lng: position.lon}}, (results, status) => {
        if (status === 'OK') {
          if (results[0]) {
            let place = results[0]
            let country = null
            place.address_components.forEach((el) => {
              el.types.forEach((t) => {
                if (t === 'country') {
                  country = el.short_name
                }
              })
            })
            let address = results[0].formatted_address
            this.$emit('positionGeocoded', {
              address: address,
              country: country,
              lat: position.lat,
              lon: position.lon
            })
          } else {
            window.alert('No results found')
            this.$emit('geocodingNotFound')
          }
        } else {
          window.alert('Geocoder failed due to: ' + status)
          this.$emit('geocodingError')
        }
      })
    }
  },
  watch: {
    googleMaps (n) {
      if (n) {
        this.geocoder = new this.googleMaps.Geocoder
      }
    }
  }
}
