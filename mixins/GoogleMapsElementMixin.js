/**
 * Google maps element base mixin
 * 
 * Importing components should call initGoogleMap(el) after googleMapsLoaded event.
 * 
 * @mixin
 */

export default {
  props: {
    mapZoom: {
      type: Number,
      default: 6
    },
    mapDebounceTimeout: {
      type: Number,
      default: 1999
    },
    mapCenter: {
      type: Object,
      default () {
        return {
          lat: 45.4687175,
          lng: 10.527247999999986
        }
      }
    },
    mapDraggable: {
      type: Boolean,
      default: true
    },
    mapNoClear: {
      type: Boolean,
      default: true
    },
    mapDisableDefaultUi: {
      type: Boolean,
      default: true
    },
    mapGestureHandling: {
      type: Boolean,
      default: true
    },
    mapZoomControl: {
      type: Boolean,
      default: true
    },
    mapScrollWheel: {
      type: Boolean,
      default: true
    },
    mapDisableDoubleClickZoom: {
      type: Boolean,
      default: false
    }
  },
  data () {
    return {
      googleMaps: null,
      googleMap: null,
      
      lastMapEvent: new Date().getTime(),

      _previousMapZoom: this.mapZoom,
      mapZoomingIn: false,
      mapZoomingOut: false
    }
  },
  mounted () {
    this.$googleMapsLoaded().then((m) => {
      this.googleMaps = m
    }, () => {
      throw 'GoogleMapsLoadingError'
    })
  },
  methods: {
    mapCanEmitEvents () {
      let can = (new Date().getTime() - this.lastMapEvent) > this.mapDebounceTimeout
      return can
    },
    initGoogleMap (el) {
      this.googleMap = new this.googleMaps.Map(el, {
        zoom: this.mapZoom,
        center: this.mapCenter,
        disableDefaultUI: this.mapDisableDefaultUi,
        draggable: this.mapDraggable,
        noClear: this.mapNoClear,
        gestureHandling: this.mapGestureHandling,
        zoomControl: this.mapZoomControl,
        scrollwheel: this.mapScrollWheel,
        disableDoubleClickZoom: this.mapDisableDoubleClickZoom
      })
    },
    panTo (lat, lon) {
      let latLng = new this.googleMaps.LatLng(lat, lon)
      this.googleMap.panTo(latLng)
    },
    setZoom (z) {
      this.googleMap.setZoom(z)
    },
    mapPositionClicked (lat, lon) {
      this.$emit('mapPositionClicked', {
        lat: lat,
        lon: lon
      })
    },
    disableUserMapActions () {
      if (this.googleMap) {
        this.googleMap.setOptions({draggable: false, zoomControl: false, scrollwheel: false, disableDoubleClickZoom: true, gestureHandling: false})
      }
    },
    enableUserMapActions () {
      if (this.googleMap) {
        this.googleMap.setOptions({draggable: this.mapDraggable, zoomControl: this.mapZoomControl, scrollwheel: this.mapScrollWheel, disableDoubleClickZoom: this.mapDisableDoubleClickZoom, gestureHandling: this.mapGestureHandling})
      }
    },
    updateLastMapEvent () {
      this.lastMapEvent = new Date().getTime()
      // window.lte = this.lastMapEvent
    }
  },
  watch: {
    googleMaps (n) {
      if (n) {
        this.$emit('mapsLoaded', this.googleMaps)
      }
    },
    googleMap (n) {
      if (n) {
        this.googleMaps.event.addListenerOnce(this.googleMap, 'idle', () => {
          // console.log('idle')
          this.$emit('mapIdle')
        })

        this.googleMaps.event.addListener(this.googleMap, 'click', (event) => {
          // console.log('click')
          let lat = event.latLng.lat()
          let lon = event.latLng.lng()

          setTimeout(() => {
            if (this.markerClusterClicked) {
              // click on cluster
              this.markerClusterClicked = false
            } else {
              // 'no clusterer, click on map'
              this.mapPositionClicked(lat, lon)
            }
          }, 0)
        })

        this.googleMaps.event.addListener(this.googleMap, 'center_changed', () => {
          // console.log('center_changed')
          let c = this.googleMap.getCenter()

          this.$emit('mapCenterChanged', {lat: c.lat(), lon: c.lng()})
        })

        this.googleMaps.event.addListener(this.googleMap, 'zoom_changed', () => {
          // console.log('zoom_changed')
          let z = this.googleMap.getZoom()
          
          if (z > this._previousMapZoom) {
            this.mapZoomingIn = true
            this.mapZoomingOut = false
          } else {
            this.mapZoomingIn = false
            this.mapZoomingOut = (z !== this._previousMapZoom)
          }
          this._previousMapZoom = z

          this.$emit('mapZoomChanged', z)
        })

        this.$emit('mapLoaded', this.googleMap)
      }
    }
  }
}
