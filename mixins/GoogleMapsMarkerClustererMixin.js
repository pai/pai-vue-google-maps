import MarkerClusterer from '@google/markerclusterer'

export default {
  data () {
    return {
      markerClusterer: true,
      markerClusterStarted: false,
      startingMarkerClusters: false,
      markerClusterTypes: {
        cluster: {
          markerClustererImagePath: 'https://cdn.q-cumber.org/static/entities/event/map/markerclusterer/m'
        }
      },
      markerClusters: {}
    }
  },
  methods: {
    getMarkerClustererImagePath (k) {
      let clusterType = this.markerClusterTypes[k]
      if (clusterType) {
        return clusterType.markerClustererImagePath
      } else {
        return 'https://cdn.q-cumber.org/static/entities/event/map/markerclusterer/m'
      }
    },
    initMarkerClusters () {
      this.startingMarkerClusters = true

      for (let k in this.markerClusterTypes) {
        if (this.markerClusterTypes.hasOwnProperty(k)) {
          this.markerClusters[k] = new MarkerClusterer(
            this.googleMap,
            [],
            {
              imagePath: this.getMarkerClustererImagePath(k),
              maxZoom: 18,
              zoomOnClick: true
            }
          )

          // disable marker clusterer click event propagation
          this.googleMaps.event.addListener(this.markerClusters[k], 'clusterclick', () => { // (cluster)
            this.markerClusterClicked = true
            //console.log('clusterclick', k)
          })
        }
      }
      this.startingMarkerClusters = false
      this.markerClusterStarted = true
    },
    addMarkerToCluster (m, k) {
      if (this.markerClusters[k]) {
        this.markerClusters[k].addMarker(m)
      }
    },
    removeAllMarkersFromCluster () {
      for (let k in this.markerClusterTypes) {
        if (this.markerClusterTypes.hasOwnProperty(k)) {
          if (this.markerClusters[k]) {
            this.markerClusters[k].clearMarkers()
          }
        }
      }
    }
  },
  watch: {
    googleMap () {
      if (!this.markerClusterStarted && !this.startingMarkerClusters) {
        this.initMarkerClusters()
      }
    }
  }
}
