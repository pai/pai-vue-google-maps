/*
  VueJs Google Maps Plugin
*/

const loadGoogleMaps = (options) => {
  return new Promise((resolve, reject) => {
    window['initGoogleMaps'] = resolve

    let GoogleMapsScript = document.createElement('script')
    GoogleMapsScript.type = 'text/javascript'
    GoogleMapsScript.setAttribute('src', 'https://maps.googleapis.com/maps/api/js?key=' + options.apiKey + '&libraries=' + options.libraries + '&callback=initGoogleMaps')

    document.body.appendChild(GoogleMapsScript)
  })
}

const getGoogleMaps = () => {
  return new Promise((resolve, reject) => {
    if (typeof window !== 'undefined' && !window.loadingGoogleMaps && window.google && window.google.maps) {
      resolve(window.google.maps)
    } else {
      document.addEventListener('googleMapsLoaded', (ev) => {
        if (ev.detail) {
          resolve(ev.detail)
        } else {
          reject("Error loading google maps.")
        }
      })
    }
  })
}

const VueGoogleMaps = {
  install (Vue, options) {

    let _options = {
      apiKey: 'xxx',
      libraries: 'places'
    }

    if (options) {
      if (options.apiKey) {
        _options.apiKey = options.apiKey
      }
      if (options.libraries) {
        _options.libraries = options.libraries
      }
    }

    Vue.mixin({
      mounted () {
        Vue.prototype.$googleMapsLoaded = getGoogleMaps
      }
    })

    loadGoogleMaps(_options).then(
	  (maps) => {
        let event = new CustomEvent('googleMapsLoaded', {
          detail: google.maps
        })
        document.dispatchEvent(event)
      },
      (err) => {
        throw err
      }
    )
  }
}

export default VueGoogleMaps
