export default {
  data () {
    return {
      mapBoundsUpdated: false,
      mapBounds: null,
      lastMapBounds: null,
      mapDragging: false
    }
  },
  methods: {
    getMapBounds () {
      let bounds = this.googleMap.getBounds()
      let ne = bounds.getNorthEast()
      let sw = bounds.getSouthWest()

      return {
        ne: {
          lat: ne.lat(),
          lon: ne.lng()
        },
        sw: {
          lat: sw.lat(),
          lon: sw.lng()
        }
      }
    },
    updateMapBounds () {
      this.lastMapBounds = this.mapBounds
      this.mapBounds = this.getMapBounds()

      this.$emit('mapBoundsChanged', this.mapBounds)
    }
  },
  watch: {
    googleMap () {
      this.googleMaps.event.addListener(this.googleMap, 'bounds_changed', () => {
        if (this.mapDragging) {
          this.mapWasDragging = true
        } else {
          this.updateMapBounds()
        }
      })

      this.googleMaps.event.addListener(this.googleMap, 'dragstart', () => {
        this.mapDragging = true
      })
      this.googleMaps.event.addListener(this.googleMap, 'dragend', () => {
        this.mapDragging = false
        if (this.mapWasDragging) {
          this.mapWasDragging = false
          this.updateMapBounds()
        }
      })
    }
  }
}
