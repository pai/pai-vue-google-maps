import { OverlappingMarkerSpiderfier } from 'npm-overlapping-marker-spiderfier/lib/oms.min'

export default {
  data () {
    return {
      markerClusterer: true,
      markerClusterClicked: false,
      markerSpiderfier: null,
      markerSpiderfierClicked: false,
      mapClusterSpiderOpened: false
    }
  },
  methods: {
    initMarkerSpiderified () {
      this.markerSpiderfier = new OverlappingMarkerSpiderfier(this.googleMap, {
        markersWontMove: true,
        markersWontHide: true,
        basicFormatEvents: false,
        ignoreMapClick: true,
        keepSpiderfied: true
      })

      this.markerSpiderfier.addListener('click', (marker, e) => {
        this.markerClusterClicked = true
      })

      this.markerSpiderfier.addListener('spiderfy', (markers) => {
        // console.log('spiderfy', markers)
        this.markerClusterClicked = true
        this.mapClusterSpiderOpened = true
        this.$emit('overlappingMarkerSpiderfy', markers)
      })
      this.markerSpiderfier.addListener('unspiderfy', (markers) => {
        // console.log('unspiderify')
        this.mapClusterSpiderOpened = false
        this.$emit('overlappingMarkerUnspiderfy', markers)
      })
    },
    addMarkerToSpiderfier (m) {
      this.markerSpiderfier.addMarker(m)
    },
    removeAllMarkersFromSpiderfier () {
      this.markerSpiderfier.removeAllMarkers()
    }
  },
  watch: {
    googleMap () {
      if (!this.markerSpiderfier) {
        this.initMarkerSpiderified()
      }
    }
  }
}
