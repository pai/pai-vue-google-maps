import LoadingElementMixin from '@paiuolo/pai-vue-mixins/mixins/LoadingElementMixin'
import GoogleMapsElementMixin from '@paiuolo/pai-vue-google-maps/mixins/GoogleMapsElementMixin'

export default {
  mixins: [
    LoadingElementMixin,
    GoogleMapsElementMixin
  ],
  props: {
    watchBoundMap: {
      type: Boolean,
      default: false
    },
    boundMap: {
      type: Object,
      default: null
    }
  },
  data () {
    return {
      autocomplete: null,
      addressInput: null,
      computedPlace: null
    }
  },
  methods: {
    clear () {
      this.addressInput = null
      this.computedPlace = null
      this.$emit('placeChanged', null)
    },
    initPlaceInput () {
      if (!this.loading) {
        this.startLoading()
        if (!this.autocomplete) {
          let placeInput = this.getInputElement()
          this.autocomplete = new this.googleMaps.places.Autocomplete(placeInput)

          if (this.boundMap) {
            // Bind the map's bounds (viewport) property to the autocomplete object,
            // so that the autocomplete requests use the current map bounds for the
            // bounds option in the request.
            this.autocomplete.bindTo('bounds', this.boundMap)
          }
          // Set the data fields to return when the user selects a place.
          this.autocomplete.setFields(['address_components', 'geometry', 'icon', 'name', 'formatted_address'])

          this.autocomplete.addListener('place_changed', this.placeChanged)
        }
        this.stopLoading()
      }
    },
    placeChanged () {
      let place = this.autocomplete.getPlace()

      if (!place.geometry) {
        // User entered the name of a Place that was not suggested and
        // pressed the Enter key, or the Place Details request failed.
        window.alert("No details available for input: '" + place.name + "'")
        return
      }

      this.parsePlace(place)
    },
    parsePlace (place) {
      if (place && place.address_components) {
        // console.log('parsePlace', place)
        let country = null
        place.address_components.forEach((el) => {
          el.types.forEach((t) => {
            if (t === 'country') {
              country = el.short_name
            }
          })
        })

        let address = place.formatted_address
        /*if (place.address_components) {
          address = [
            (place.address_components[0] && place.address_components[0].short_name) || '',
            (place.address_components[1] && place.address_components[1].short_name) || '',
            (place.address_components[2] && place.address_components[2].short_name) || ''
          ].join(' ')
        }*/

        this.computedPlace = {
          address: address,
          country: country,
          lat: place.geometry.location.lat(),
          lon: place.geometry.location.lng()
        }
      } else {
        this.computedPlace = null
      }

      this.$emit('placeChanged', this.computedPlace)
    },
    getInputElement () {
      // should be overriden
      return this.$refs.input
    }
  },
  watch: {
    boundMap () {
      // console.log('boundMap has changed', n, o)
      this.initPlaceInput()
    },
    googleMaps () {
      if (this.watchBoundMap && !this.boundMap) {
        // console.log('boundMap loading, waiting for change..')
      } else {
        this.initPlaceInput()
      }
    }
  }
}
